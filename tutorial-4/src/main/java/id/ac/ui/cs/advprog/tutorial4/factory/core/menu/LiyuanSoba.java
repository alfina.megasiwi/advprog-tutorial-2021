package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaIngredientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.RamenIngredientsFactory;

public class LiyuanSoba extends Menu {
    //Ingridients:
    //Noodle: Soba
    //Meat: Beef
    //Topping: Mushroom
    //Flavor: Sweet
    private RamenIngredientsFactory ingredientsFactory;
    public LiyuanSoba(String name){
        super(name, new LiyuanSobaIngredientsFactory());
    }
}