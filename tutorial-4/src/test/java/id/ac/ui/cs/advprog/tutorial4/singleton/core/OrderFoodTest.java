package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class OrderFoodTest {
    private Class<?> orderFoodClass;

    @InjectMocks
    private OrderFood orderFood;

    @BeforeEach
    public void setUp() throws Exception {
        orderFoodClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood");
        orderFood = OrderFood.getInstance();
    }

    @Test
    public void testIsOrderFoodConcreteClass() {
        assertFalse(Modifier.
                isAbstract(orderFoodClass.getModifiers()));
    }

    @Test
    public void testOrderFoodHasGetFoodMethod() throws Exception {
        Method getFood = orderFoodClass.getDeclaredMethod("getFood");

        assertEquals("java.lang.String",
                getFood.getGenericReturnType().getTypeName());
        assertEquals(0,
                getFood.getParameterCount());
        assertTrue(Modifier.isPublic(getFood.getModifiers()));
    }

    @Test
    public void testOrderFoodHasSetFoodMethod() throws Exception {
        Class<?>[] setFoodArgs = new Class[1];
        setFoodArgs[0] = String.class;
        Method setFood = orderFoodClass.getDeclaredMethod("setFood", setFoodArgs);

        assertEquals(1,
                setFood.getParameterCount());
        assertTrue(Modifier.isPublic(setFood.getModifiers()));
    }

    @Test
    public void testOrderFoodHasToStringMethod() throws Exception {
        Method toString = orderFoodClass.getDeclaredMethod("toString");

        assertEquals("java.lang.String",
                toString.getGenericReturnType().getTypeName());
        assertEquals(0,
                toString.getParameterCount());
        assertTrue(Modifier.isPublic(toString.getModifiers()));
    }

    @Test
    public void testOrderFoodHasGetInstanceMethod() throws Exception {
        Method getInstance = orderFoodClass.getDeclaredMethod("getInstance");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood",
                getInstance.getGenericReturnType().getTypeName());
        assertEquals(0,
                getInstance.getParameterCount());
        assertTrue(Modifier.isPublic(getInstance.getModifiers()));
    }

    @Test
    public void testOrderFoodGetDrinkMethod() {
        orderFood.setFood("sate");
        String result = orderFood.getFood() ;
        assertEquals("sate", result);
    }

    @Test
    public void testOrderFoodToStringMethod() {
        orderFood.setFood("air putih");
        String result = orderFood.toString() ;
        assertEquals("air putih", result);
    }

    @Test
    public void testOrderFoodGetInstanceMethod() {
        OrderFood orderFood2 = OrderFood.getInstance();
        assertEquals(orderFood, orderFood2);
    }
}
