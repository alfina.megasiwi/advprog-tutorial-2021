package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: add tests
public class SnevnezhaShiratakiIngredientsFactoryTest {
    private Class<?> snevnezhaShiratakiIngredientsFactoryClass;

    @InjectMocks
    private SnevnezhaShiratakiIngredientsFactory snevnezhaShiratakiIngredientsFactory;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiIngredientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiIngredientsFactory");
        snevnezhaShiratakiIngredientsFactory = new SnevnezhaShiratakiIngredientsFactory();
    }

    @Test
    public void testIsSnevnezhaShiratakiIngredientsFactoryConcreteClass() {
        assertFalse(Modifier.
                isAbstract(snevnezhaShiratakiIngredientsFactoryClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientsFactoryIsARamenIngredientsFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiIngredientsFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.RamenIngredientsFactory")));
    }

//    public Flavor createFlavor();
//    public Meat createMeat();
//    public Noodle createNoodle();
//    public Topping createTopping();

    @Test
    public void testSnevnezhaShiratakiIngredientsFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = snevnezhaShiratakiIngredientsFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientsFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = snevnezhaShiratakiIngredientsFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientsFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = snevnezhaShiratakiIngredientsFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientsFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = snevnezhaShiratakiIngredientsFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientsFactoryCreateFlavorReturn() throws Exception{
        assertTrue(snevnezhaShiratakiIngredientsFactory.createFlavor() instanceof Umami);
    }

    @Test
    public void testSnevnezhaShiratakiIngredientsFactoryCreateMeatReturn() throws Exception{
        assertTrue(snevnezhaShiratakiIngredientsFactory.createMeat() instanceof Fish);
    }

    @Test
    public void testSnevnezhaShiratakiIngredientsFactoryCreateNoodleReturn() throws Exception{
        assertTrue(snevnezhaShiratakiIngredientsFactory.createNoodle() instanceof Shirataki);
    }

    @Test
    public void testSnevnezhaShiratakiIngredientsFactoryCreateToppingReturn() throws Exception{
        assertTrue(snevnezhaShiratakiIngredientsFactory.createTopping() instanceof Flower);
    }
}