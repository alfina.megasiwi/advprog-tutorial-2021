package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: add tests
public class MondoUdonTest {
    private Class<?> mondoUdonClass;

    @InjectMocks
    private MondoUdon mondoUdon;

    //    createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba");
//    createMenu("Bakufu Spicy Pork Ramen", "InuzumaRamen");
//    createMenu("Good Hunter Cheese Chicken Udon", "MondoUdon");
//    createMenu("Morepeko Flower Fish Shirataki", "SnevnezhaShirataki");
    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
        mondoUdon = new MondoUdon("WanPlus Beef Mushroom Soba");
    }

    @Test
    public void testIsMondoUdonConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonCreateFlavorReturn() throws Exception{
        assertTrue(mondoUdon.getFlavor() instanceof Salty);
    }

    @Test
    public void testMondoUdonCreateMeatReturn() throws Exception{
        assertTrue(mondoUdon.getMeat() instanceof Chicken);
    }

    @Test
    public void testMondoUdonCreateNoodleReturn() throws Exception{
        assertTrue(mondoUdon.getNoodle() instanceof Udon);
    }

    @Test
    public void testMondoUdonIngredientsFactoryCreateToppingReturn() throws Exception{
        assertTrue(mondoUdon.getTopping() instanceof Cheese);
    }
}
