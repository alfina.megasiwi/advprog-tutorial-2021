package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: add tests
public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;

    @InjectMocks
    private LiyuanSoba liyuanSoba;

    //    createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba");
//    createMenu("Bakufu Spicy Pork Ramen", "InuzumaRamen");
//    createMenu("Good Hunter Cheese Chicken Udon", "MondoUdon");
//    createMenu("Morepeko Flower Fish Shirataki", "SnevnezhaShirataki");
    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
        liyuanSoba = new LiyuanSoba("WanPlus Beef Mushroom Soba");
    }

    @Test
    public void testIsLiyuanSobaConcreteClass() {
        assertFalse(Modifier.
                isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaCreateFlavorReturn() throws Exception{
        assertTrue(liyuanSoba.getFlavor() instanceof Sweet);
    }

    @Test
    public void testLiyuanSobaCreateMeatReturn() throws Exception{
        assertTrue(liyuanSoba.getMeat() instanceof Beef);
    }

    @Test
    public void testLiyuanSobaCreateNoodleReturn() throws Exception{
        assertTrue(liyuanSoba.getNoodle() instanceof Soba);
    }

    @Test
    public void testLiyuanSobaIngredientsFactoryCreateToppingReturn() throws Exception{
        assertTrue(liyuanSoba.getTopping() instanceof Mushroom);
    }
}
