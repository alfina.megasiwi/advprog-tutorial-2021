package id.ac.ui.cs.advprog.tutorial3.facade.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class MenuServiceImplTest {

    private MenuServiceImpl menuService;

    @Mock
    private MenuRepository menuRepository;

    private Menu menu;

    @BeforeEach
    public void setup() throws Exception {

        menuService = new MenuServiceImpl();
        menu = new LiyuanSoba("sobami");
        menuRepository.add(menu);
    }

    @Test
    public void whenMenuServiceGetMenusShouldReturnMenuList() {
        ReflectionTestUtils.setField(menuService, "repo", menuRepository);
        List<Menu> acquiredMenu = menuService.getMenus();

        assertThat(acquiredMenu).isEqualTo(menuRepository.getMenus());
    }


}
