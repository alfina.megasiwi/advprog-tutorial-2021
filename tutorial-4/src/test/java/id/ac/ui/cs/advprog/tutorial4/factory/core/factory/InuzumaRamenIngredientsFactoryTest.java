package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: add tests
public class InuzumaRamenIngredientsFactoryTest {
    private Class<?> inuzumaRamenIngredientsFactoryClass;

    @InjectMocks
    private InuzumaRamenIngredientsFactory inuzumaRamenIngredientsFactory;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenIngredientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenIngredientsFactory");
        inuzumaRamenIngredientsFactory = new InuzumaRamenIngredientsFactory();
    }

    @Test
    public void testIsInuzumaRamenIngredientsFactoryConcreteClass() {
        assertFalse(Modifier.
                isAbstract(inuzumaRamenIngredientsFactoryClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIngredientsFactoryIsARamenIngredientsFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenIngredientsFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.RamenIngredientsFactory")));
    }

//    public Flavor createFlavor();
//    public Meat createMeat();
//    public Noodle createNoodle();
//    public Topping createTopping();

    @Test
    public void testInuzumaRamenIngredientsFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = inuzumaRamenIngredientsFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIngredientsFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = inuzumaRamenIngredientsFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIngredientsFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = inuzumaRamenIngredientsFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIngredientsFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = inuzumaRamenIngredientsFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIngredientsFactoryCreateFlavorReturn() throws Exception{
        assertTrue(inuzumaRamenIngredientsFactory.createFlavor() instanceof Spicy);
    }

    @Test
    public void testInuzumaRamenIngredientsFactoryCreateMeatReturn() throws Exception{
        assertTrue(inuzumaRamenIngredientsFactory.createMeat() instanceof Pork);
    }

    @Test
    public void testInuzumaRamenIngredientsFactoryCreateNoodleReturn() throws Exception{
        assertTrue(inuzumaRamenIngredientsFactory.createNoodle() instanceof Ramen);
    }

    @Test
    public void testInuzumaRamenIngredientsFactoryCreateToppingReturn() throws Exception{
        assertTrue(inuzumaRamenIngredientsFactory.createTopping() instanceof BoiledEgg);
    }
}
