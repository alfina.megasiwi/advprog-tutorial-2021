package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: add tests
public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;

    @InjectMocks
    private SnevnezhaShirataki snevnezhaShirataki;

    //    createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba");
//    createMenu("Bakufu Spicy Pork Ramen", "InuzumaRamen");
//    createMenu("Good Hunter Cheese Chicken Udon", "MondoUdon");
//    createMenu("Morepeko Flower Fish Shirataki", "SnevnezhaShirataki");
    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
        snevnezhaShirataki = new SnevnezhaShirataki("WanPlus Beef Mushroom Soba");
    }

    @Test
    public void testIsSnevnezhaShiratakiConcreteClass() {
        assertFalse(Modifier.
                isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiCreateFlavorReturn() throws Exception{
        assertTrue(snevnezhaShirataki.getFlavor() instanceof Umami);
    }

    @Test
    public void testSnevnezhaShiratakiCreateMeatReturn() throws Exception{
        assertTrue(snevnezhaShirataki.getMeat() instanceof Fish);
    }

    @Test
    public void testSnevnezhaShiratakiCreateNoodleReturn() throws Exception{
        assertTrue(snevnezhaShirataki.getNoodle() instanceof Shirataki);
    }

    @Test
    public void testSnevnezhaShiratakiIngredientsFactoryCreateToppingReturn() throws Exception{
        assertTrue(snevnezhaShirataki.getTopping() instanceof Flower);
    }
}
