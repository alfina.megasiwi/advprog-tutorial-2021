package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.Summary;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.Duration;

import java.util.ArrayList;
import java.util.HashMap;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    private HashMap<String, Summary> hastLogPerBulan;

    public LogServiceImpl(){
        hastLogPerBulan = new HashMap<String, Summary>();
    }

    @Override
    public Log createLog(Log log) {
        logRepository.save(log);
        return log;
    }

    @Override
    public Iterable<Log> getListLog() {
        return logRepository.findAll();
    }

    @Override
    public Log getLogByidLog(int idLog) {
        return logRepository.findByidLog(idLog);
    }

    @Override
    public Log updateLog(int idLog, Log log) {
        log.setIdLog(idLog);
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLogByidLog(int idLog) {
        logRepository.deleteById(idLog);
    }

    @Override
    public Log updateMahasiswa(String npm, Log log){
        log.setMahasiswa(mahasiswaRepository.findByNpm(npm));
        logRepository.save(log);
        return log;
    }

    @Override
    public Summary laporanPembayaran(String npm, String bulan){
        Mahasiswa mahasiswaA = mahasiswaRepository.findByNpm(npm);
        List<Log> logMahasiswaA = mahasiswaA.getLogList();

        double jamKerjaTotal = 0;
        double pembayaran = 0;

        for (Log log : logMahasiswaA) {
            String bulanLog = log.getStartTime().getMonth().name();
            Duration durasiKerjaLog = Duration.between(log.getStartTime(), log.getEndTime());
            if(bulanLog.equalsIgnoreCase(bulan)) {
                double jamKerjaLog = (double) durasiKerjaLog.toMinutes()/60;
                jamKerjaTotal += jamKerjaLog;
                pembayaran += (350 * jamKerjaLog);
            }
        }
        Summary summaryBulanIni = new Summary(bulan, jamKerjaTotal, pembayaran);
        return summaryBulanIni;
    }

}
