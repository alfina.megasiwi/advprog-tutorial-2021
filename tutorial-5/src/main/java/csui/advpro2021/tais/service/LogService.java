package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.Summary;

public interface LogService {
    Log createLog(Log log);

    Iterable<Log> getListLog();

    Log getLogByidLog(int idLog);

    Log updateLog(int idLog, Log log);

    void deleteLogByidLog(int idLog);

    Log updateMahasiswa(String npm, Log log);

    Summary laporanPembayaran(String npm, String bulan);
}
