package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.Summary;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postLog(@RequestBody Log log) {
        return ResponseEntity.ok(logService.createLog(log));
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLog() {
        return ResponseEntity.ok(logService.getListLog());
    }

    @GetMapping(path = "/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLog(@PathVariable(value = "idLog") int idLog) {
        Log log = logService.getLogByidLog(idLog);
        if (log == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(log);
    }

    @PutMapping(path = "/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "idLog") int idLog, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(idLog, log));
    }

    @DeleteMapping(path = "/{idLog}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "idLog") int idLog) {
        logService.deleteLogByidLog(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping(path = "/{idLog}/dicatat/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateMahasiswa(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateMahasiswa(npm, log));
    }

    @GetMapping(path = "/laporanPembayaran/{npm}/bulan/{bulan}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLaporanPembayaran (@PathVariable(value = "npm") String npm, @PathVariable(value = "bulan") String bulan) {
        Summary summary = logService.laporanPembayaran(npm, bulan);
        if (summary == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(summary);
    }
}
