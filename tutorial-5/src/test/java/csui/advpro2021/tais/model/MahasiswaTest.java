package csui.advpro2021.tais.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

public class MahasiswaTest {
    private Mahasiswa mahasiswa;

    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp() throws Exception {
        this.mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4", "081317691718");
        this.mataKuliah = new MataKuliah("ADVPROG", "Advanced Programming", "Ilmu Komputer");
        this.mahasiswa.setMataKuliah(mataKuliah);
    }

    @Test
    public void testRelationshipMahasiswaMatkul() {
        Assertions.assertEquals(mataKuliah, mahasiswa.getMataKuliah());
    }
}